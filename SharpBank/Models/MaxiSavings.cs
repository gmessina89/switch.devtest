﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.Models
{
    public class MaxiSavings : Account
    {

        public MaxiSavings() : base()
        {
        }

        public override string GetAccountType()
        {
            return "Maxi Savings Account";
        }

        public override double DailyInterest()
        {
            {
                double amount = 0;
                double interest = 0;

                double currentAmount = 0;           

                for (int i = 0; i < transactions.Count; i++)
                {
                    double difference = 0;      
                    currentAmount += transactions[i].amount;
                    if (i + 1 != transactions.Count  )
                    {      
                        if(transactions[i].amount > 0)
                        {
                            difference = (transactions[i + 1].transactionDate - transactions[i].transactionDate).Days;
                            interest = difference * 0.05 / 365;
                            amount += currentAmount * interest;
                            currentAmount += currentAmount * interest;
                        }
                        else
                        {
                            if ((DateTime.Now - transactions[i].transactionDate).Days > 10)
                            {
                                difference = (transactions[i+1].transactionDate - transactions[i].transactionDate).Days;
                                interest = 10 * 0.001 / 365;
                                amount += currentAmount * interest;
                                currentAmount += amount;
                                interest = (difference - 10) * 0.05 / 365;
                                amount += currentAmount * interest;
                                currentAmount += currentAmount * interest;
                            }
                            else
                            {
                                difference = (transactions[i + 1].transactionDate - transactions[i].transactionDate).Days;
                                interest = difference * 0.001 / 365;
                                amount += currentAmount * interest;
                                currentAmount += currentAmount * interest;
                            }
                        }
                       

                    }
                    else
                    {
                        if (transactions[i].amount > 0)
                        {
                            difference = (DateTime.Now - transactions[i].transactionDate).Days;
                            interest = difference * 0.05 / 365;
                            amount += currentAmount * interest;
                            currentAmount += currentAmount * interest;
                        }
                        else
                        {
                            if ((DateTime.Now - transactions[i].transactionDate).Days > 10)
                            {
                                difference = (DateTime.Now - transactions[i].transactionDate).Days;
                                interest = 10 * 0.001 / 365;
                                amount += currentAmount * interest;                               
                                interest = (difference - 10) * 0.05 / 365;
                                amount += currentAmount * interest;
                                currentAmount += currentAmount * interest;
                            }
                            else
                            {
                                difference = (DateTime.Now - transactions[i].transactionDate).Days;
                                interest = difference * 0.001 / 365;
                                amount += currentAmount * interest;
                                currentAmount += currentAmount * interest;
                            }
                        }
                    }
                }
                return amount;
            }
        }

    }
}
