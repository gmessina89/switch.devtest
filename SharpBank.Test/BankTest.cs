﻿using NUnit.Framework;
using SharpBank.Models;
using System;

namespace SharpBank.Test
{
    [TestFixture]
    public class BankTest
    {
        private static readonly double DOUBLE_DELTA = 1e-15;

        [Test]
        public void CustomerSummary()
        {
            Bank bank = new Bank();
            Customer john = new Customer("John");
            john.OpenAccount(new Checking());
            bank.AddCustomer(john);

            Assert.AreEqual("Customer Summary\n - John (1 account)", bank.CustomerSummary());
        }

        [Test]
        public void CheckingAccount()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Checking();
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);

            checkingAccount.Deposit(100.0);
            checkingAccount.transactions[0].transactionDate = DateTime.Now.AddDays(-365);

            Assert.AreEqual(0.1, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void CheckingAccountMultiTransactions()
        {
            Bank bank = new Bank();
            Account checkingAccount = new Checking();
            Customer bill = new Customer("Bill").OpenAccount(checkingAccount);
            bank.AddCustomer(bill);
            checkingAccount.Deposit(3000.0);
            checkingAccount.transactions[0].transactionDate = DateTime.Now.AddDays(-1095);
            checkingAccount.Withdraw(1000.0);
            checkingAccount.transactions[1].transactionDate = DateTime.Now.AddDays(-730);
            checkingAccount.Deposit(2000.0);
            checkingAccount.transactions[2].transactionDate = DateTime.Now.AddDays(-365);
            

            Assert.AreEqual(9.01, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccount()
        {
            Bank bank = new Bank();
            Account savingsAccount = new Savings();
            bank.AddCustomer(new Customer("Bill").OpenAccount(savingsAccount));

            savingsAccount.Deposit(1500.0);
            savingsAccount.transactions[0].transactionDate = DateTime.Now.AddDays(-365);

            Assert.AreEqual(2.0, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void SavingsAccountMultiTransactions()
        {
            Bank bank = new Bank();
            Account savingsAccount = new Savings();
            bank.AddCustomer(new Customer("Bill").OpenAccount(savingsAccount));

            savingsAccount.Deposit(1500.0);
            savingsAccount.transactions[0].transactionDate = DateTime.Now.AddDays(-730);
            savingsAccount.Withdraw(600.0);
            savingsAccount.transactions[1].transactionDate = DateTime.Now.AddDays(-365);

            Assert.AreEqual(2.9, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccount()
        {
            Bank bank = new Bank();
            Account maxiAccount = new MaxiSavings();
            bank.AddCustomer(new Customer("Bill").OpenAccount(maxiAccount));  
            
            maxiAccount.Deposit(3000.0);
            maxiAccount.transactions[0].transactionDate = DateTime.Now.AddDays(-365);

            Assert.AreEqual(150, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccountWithoutWithdrawalInPastThan10Days()
        {
            Bank bank = new Bank();
            Account maxiAccount = new MaxiSavings();
            bank.AddCustomer(new Customer("Bill").OpenAccount(maxiAccount));

            maxiAccount.Deposit(4000.0);
            maxiAccount.transactions[0].transactionDate = DateTime.Now.AddDays(-730);
            maxiAccount.Withdraw(1000.0);
            maxiAccount.transactions[1].transactionDate = DateTime.Now.AddDays(-365);

            Assert.AreEqual(355.70, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

        [Test]
        public void MaxiSavingsAccountWithWithdrawalInPast10Days()
        {
            Bank bank = new Bank();
            Account maxiAccount = new MaxiSavings();
            bank.AddCustomer(new Customer("Bill").OpenAccount(maxiAccount));

            maxiAccount.Deposit(4000.0);
            maxiAccount.transactions[0].transactionDate = DateTime.Now.AddDays(-400);
            maxiAccount.Withdraw(1000.0);
            maxiAccount.transactions[1].transactionDate = DateTime.Now.AddDays(-9);

            Assert.AreEqual(214.33, bank.TotalInterestPaid(), DOUBLE_DELTA);
        }

    }
}
