﻿using NUnit.Framework;
using SharpBank.Models;

namespace SharpBank.Test
{
    [TestFixture]
    public class CustomerTest
    {

        [Test]
        public void TestCustomerStatementGeneration()
        {
            Account checkingAccount = new Checking();
            Account savingsAccount = new Savings();
            Account maxiAccount = new MaxiSavings();

            Customer henry = new Customer("Henry").OpenAccount(checkingAccount).OpenAccount(savingsAccount).OpenAccount(maxiAccount);

            checkingAccount.Deposit(900.0);
            savingsAccount.Deposit(4000.0);
            savingsAccount.Withdraw(200.0);
            henry.Transfer(checkingAccount, maxiAccount, 300);



            henry.GetStatement();

            Assert.AreEqual("Statement for Henry\n\n" +
             "Checking Account\n  deposit $900,00\n  withdrawal $300,00\n" +
             "Total $600,00\n\n" +
             "Savings Account\n  deposit $4.000,00\n  withdrawal $200,00\n" +
             "Total $3.800,00\n\n" +
             "Maxi Savings Account\n  deposit $300,00\n" +
             "Total $300,00\n\n" +
             "Total In All Accounts $4.700,00"
             , henry.GetStatement());
        }

        [Test]
        public void TestOneAccount()
        {
            Customer oscar = new Customer("Oscar").OpenAccount(new Savings());
            Assert.AreEqual(1, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTwoAccount()
        {
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Savings());
            oscar.OpenAccount(new Checking());
            Assert.AreEqual(2, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestThreeAcounts()
        {


            Customer oscar = new Customer("Oscar")
                    .OpenAccount(new Savings());
            oscar.OpenAccount(new Checking());
            oscar.OpenAccount(new MaxiSavings());

            Assert.AreEqual(3, oscar.GetNumberOfAccounts());
        }

        [Test]
        public void TestTransferBetweenAccounts()
        {
            Account checkingAccount = new Checking();
            Account savingsAccount = new Savings();
            Customer oscar = new Customer("Oscar")
                    .OpenAccount(checkingAccount);
            oscar.OpenAccount(savingsAccount);

            checkingAccount.Deposit(1000);
            oscar.Transfer(checkingAccount, savingsAccount, 500);

            Assert.AreEqual("Statement for Oscar\n\n" +
              "Checking Account\n  deposit $1.000,00\n  withdrawal $500,00\n" +
              "Total $500,00\n\n" +
              "Savings Account\n  deposit $500,00\n" +
              "Total $500,00\n\n" +              
              "Total In All Accounts $1.000,00"
              , oscar.GetStatement());
        }
    }
}
