﻿using System;

namespace SharpBank
{
    public class Transaction
    {
        public  double amount;

        public DateTime transactionDate { get; set;}

        public Transaction(double amount)
        {
            this.amount = amount;
            this.transactionDate = DateProvider.GetInstance().Now();
        }              

    }
}
