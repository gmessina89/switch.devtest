﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank
{
    public abstract class Account
    {    
        
        public List<Transaction> transactions;

        public double balance { get; set; }

        public Account()
        {
            transactions = new List<Transaction>();
            this.balance = 0;
        }

        public void Deposit(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else
            {
                transactions.Add(new Transaction(amount));
                balance += amount;
            }
        }

        public void Withdraw(double amount)
        {
            if (amount <= 0)
            {
                throw new ArgumentException("amount must be greater than zero");
            }
            else if (balance < amount)
            {
                throw new ArgumentException("account has insufficient balance for requested action");
            }
            else
            {
                transactions.Add(new Transaction(-amount));
                balance -= amount;
            }
        }

        //**Checking accounts** have a flat rate of 0.1%
        //**Savings accounts** have a rate of 0.1% for the first $1,000 then 0.2%
        //**Maxi-Savings accounts** to have an interest rate of 5% assuming no withdrawals in the past 10 days otherwise 0.1%
        public  double InterestEarned()
        {
            double amount = DailyInterest();

            return Math.Round(amount, 2);
        }

        public abstract string GetAccountType();      

        public abstract double DailyInterest();        

    }
}
