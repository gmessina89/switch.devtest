﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.Models
{
    public class Savings : Account
    {
        public Savings() : base()
        {

        }


        public override string GetAccountType()
        {
            return "Savings Account";
        }

        public override double DailyInterest()
        {
            double amount = 0;
            double interest = 0;
            double difference = 0;
            double currentAmount = 0;

            for (int i = 0; i < transactions.Count; i++)
            {
                if (i + 1 != transactions.Count)
                {
                    difference = (transactions[i + 1].transactionDate - transactions[i].transactionDate).Days;
                }
                else
                {
                    difference = (DateTime.Now - transactions[i].transactionDate).Days;
                }
                currentAmount +=transactions[i].amount;
                if (currentAmount <= 1000)
                {
                    interest = difference * 0.001 / 365;
                    amount += currentAmount * interest;
                    currentAmount += currentAmount * interest;
                }
                else
                {
                    amount += 1000 * (difference * 0.001 / 365) + (currentAmount - 1000) * difference * 0.002 / 365;
                    currentAmount += currentAmount * interest;
                }                
            }
            return amount;

        }
      
    }
}
