﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SharpBank.Models
{
    public class Checking : Account
    {
        public Checking() : base()
        {

        }

        public override string GetAccountType()
        {
            return "Checking Account";
        }

        public override double DailyInterest()
        {
            double amount = 0;
            double currentAmount = 0;

            for (int i = 0; i < transactions.Count; i++)
            {
                double difference = 0;                
                double interest = 0.001;
                currentAmount += transactions[i].amount;
                if (i + 1 != transactions.Count)
                {
                    difference = (transactions[i + 1].transactionDate - transactions[i].transactionDate).Days;
                    interest = difference * interest / 365;
                    amount += currentAmount * interest;
                    currentAmount += currentAmount * interest;
                }
                else
                {
                    difference = (DateTime.Now - transactions[i].transactionDate).Days;
                    interest = difference * interest / 365;
                    amount += currentAmount * interest;
                    currentAmount += currentAmount * interest;
                }
            }
            return amount;
        }
        
    }
}
